﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class triviaGame : Form
    {
        private string _userName;
        private string _roomName;
        private string _numOfQuestions;
        private string _timePerQuestion;
        private int _userAnsIndex;

        private NetworkStream _clientStream;
        private byte[] bufferGet;
        private byte[] bufferSet;

        public triviaGame(NetworkStream clientStream, string username, string roomName, string numOfQuestions, string timePerQuestion)
        {
            // initializing fields
            _clientStream = clientStream;
            _userName = username;
            _roomName = roomName;
            _numOfQuestions = numOfQuestions;
            _timePerQuestion = timePerQuestion;

            InitializeComponent();

            // initializing lables
            UserNameLabel.Text = _userName;
            RoomNameLabel.Text = _roomName;
        }

        private void triviaGame_Load(object sender, EventArgs e)
        {
            StartTriviaGame();
        }

        private void StartTriviaGame()
        {
            string ansMsgCode = "219"; // an question answer code to sand to the server
            string ansIndex = "";
            int currQuestionNum = 1;
            int userScore = 0;
            ScoreLabel.Text = userScore.ToString();

            while (true)
            {
                try
                {
                    // קבלת מידע מהסרבר
                    bufferGet = new byte[3];
                    int bytesRead = _clientStream.Read(bufferGet, 0, 3); // !!!
                    string msgCode = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[3];
                    int bytesRead2 = _clientStream.Read(bufferGet, 0, 3);
                    string questionLen = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[Convert.ToInt32(questionLen)];
                    int bytesRead3 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(questionLen));
                    string question = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[3];
                    int bytesRead4 = _clientStream.Read(bufferGet, 0, 3);
                    string ans1Len = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[Convert.ToInt32(ans1Len)];
                    int bytesRead5 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans1Len));
                    string ans1 = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[3];
                    int bytesRead6 = _clientStream.Read(bufferGet, 0, 3);
                    string ans2Len = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[Convert.ToInt32(ans2Len)];
                    int bytesRead7 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans2Len));
                    string ans2 = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[3];
                    int bytesRead8 = _clientStream.Read(bufferGet, 0, 3);
                    string ans3Len = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[Convert.ToInt32(ans3Len)];
                    int bytesRead9 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans3Len));
                    string ans3 = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[3];
                    int bytesRead10 = _clientStream.Read(bufferGet, 0, 3);
                    string ans4Len = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[Convert.ToInt32(ans4Len)];
                    int bytesRead11 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(ans4Len));
                    string ans4 = new ASCIIEncoding().GetString(bufferGet);

                    // לקיחת המידע מהסרבר והשמתו בלייבלים ובכפתורים המתאימים
                    QuestionLabel.Text = question; // set the question that was sent by the server
                    NumOfQuestionsLabel.Text = "Question: " + currQuestionNum.ToString() + "/" + _numOfQuestions;
                    currQuestionNum++;

                    // set the answers thet was sent by the server
                    Answer1Button.Text = ans1;
                    Answer2Button.Text = ans2;
                    Answer3Button.Text = ans3;
                    Answer4Button.Text = ans4;

                    QuestionTimerLable.Text = _timePerQuestion; // seting the starting timer value
                    QuestionTimer.Start(); // start the timer


                    // קבלת תשובה מהיוזר ושליחתה לסרבר
                    // בדיקה על איזה כפתור היוזר לחץ
                    ansIndex = _userAnsIndex.ToString(); // get the answer index of the button the user clicked
                    if (QuestionTimerLable.Text == "0") // means thet the time is over
                    {
                        ansIndex = "5";
                    }

                    // sending to the server the question answer code (219), 
                    // the index of the answer the user chose and the time thet took the user to do so 
                    bufferSet = new ASCIIEncoding().GetBytes(ansMsgCode + (ansIndex.ToCharArray())[0] + QuestionTimerLable.Text);
                    _clientStream.Write(bufferSet, 0, bufferSet.Length);
                    _clientStream.Flush();

                    // קבלת תשובה מהסרבר אם התשובה של היוזר נכונה או לא
                    bufferGet = new byte[3];
                    int bytesRead12 = _clientStream.Read(bufferGet, 0, 3);
                    msgCode = new ASCIIEncoding().GetString(bufferGet);

                    bufferGet = new byte[1];
                    int bytesRead13 = _clientStream.Read(bufferGet, 0, 1);
                    string isAnsRight = new ASCIIEncoding().GetString(bufferGet);

                    // העלאת נקודות על תשובה נכונה
                    if (isAnsRight == "1") // the answer is right
                    {
                        userScore++;
                        ScoreLabel.Text = userScore.ToString();
                        // make the butten thet was cliked green 
                        // and make all the other ones rad 
                        // make a good answer sound
                    }
                    else // the answer is wrong
                    {
                        // make all the buttens rad
                        // except the thet is right (make him green)
                        // make a good bad answer sound
                    }
                }
                catch (Exception) { }
            }

            // סיום משחק
            // get from the server a finish message code (121)
            bufferGet = new byte[3];
            int bytesRead14 = _clientStream.Read(bufferGet, 0, 3);
            string msgCodeCloseGame = new ASCIIEncoding().GetString(bufferGet);

            // get from the server the number of users ther was in the game
            bufferGet = new byte[1];
            int bytesRead15 = _clientStream.Read(bufferGet, 0, 1);
            char numOfUsers = ((new ASCIIEncoding().GetString(bufferGet)).ToCharArray())[0];

            List<string> usersNamesAndScores = new List<string>();

            for (int j = 0; j < Convert.ToInt32(numOfUsers); j++)
            {
                bufferGet = new byte[2];
                int bytesRead16 = _clientStream.Read(bufferGet, 0, 2);
                string userNameLen = new ASCIIEncoding().GetString(bufferGet);

                bufferGet = new byte[Convert.ToInt32(userNameLen)];
                int bytesRead17 = _clientStream.Read(bufferGet, 0, Convert.ToInt32(userNameLen));
                string userName = new ASCIIEncoding().GetString(bufferGet);

                usersNamesAndScores.Add(userName); // add the user name to the list 

                bufferGet = new byte[2];
                int bytesRead18 = _clientStream.Read(bufferGet, 0, 2);
                string userScoreFromServer = new ASCIIEncoding().GetString(bufferGet);

                usersNamesAndScores.Add(userScoreFromServer); // add the user score to the list
            }

            Scores scores_window = new Scores(usersNamesAndScores);

            try
            {
                scores_window.ShowDialog();
            }
            catch (Exception) { }

            this.Hide();
            this.Close();
        }

        public void ClickedButton(object sender, EventArgs e)
        {
            // geting the name of the button thet was prassed and compering it to the other button names
            if (((sender as Button).Text) == (Answer1Button.Name))
            {
                _userAnsIndex = 1;
            }
            if (((sender as Button).Text) == (Answer2Button.Name))
            {
                _userAnsIndex = 2;
            }
            if (((sender as Button).Text) == (Answer3Button.Name))
            {
                _userAnsIndex = 3;
            }
            if (((sender as Button).Text) == (Answer4Button.Name))
            {
                _userAnsIndex = 4;
            }
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            // send to the server an exit code message (222)
            bufferSet = new ASCIIEncoding().GetBytes("222");
            _clientStream.Write(bufferSet, 0, bufferSet.Length);
            _clientStream.Flush();

            this.Hide();
            this.Close();
        }

        private void QuestionTimer_Tick(object sender, EventArgs e)
        {
            int timerValue = Convert.ToInt32(QuestionTimerLable.Text);
            timerValue--; // dercrease the timer value by 1 each second
            QuestionTimerLable.Text = timerValue.ToString();
        }
    }
}
