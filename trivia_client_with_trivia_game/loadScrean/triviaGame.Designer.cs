﻿namespace loadScrean
{
    partial class triviaGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExitButton = new System.Windows.Forms.Button();
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.RoomNameLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Answer1Button = new System.Windows.Forms.Button();
            this.Answer2Button = new System.Windows.Forms.Button();
            this.Answer3Button = new System.Windows.Forms.Button();
            this.Answer4Button = new System.Windows.Forms.Button();
            this.NumOfQuestionsLabel = new System.Windows.Forms.Label();
            this.QuestionLabel = new System.Windows.Forms.Label();
            this.QuestionTimerLable = new System.Windows.Forms.Label();
            this.ScoreLabel = new System.Windows.Forms.Label();
            this.QuestionTimer = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(697, 13);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(75, 23);
            this.ExitButton.TabIndex = 0;
            this.ExitButton.Text = "Exit";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.UserNameLabel.Location = new System.Drawing.Point(156, 87);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(66, 15);
            this.UserNameLabel.TabIndex = 1;
            this.UserNameLabel.Text = "user name";
            // 
            // RoomNameLabel
            // 
            this.RoomNameLabel.AutoSize = true;
            this.RoomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.RoomNameLabel.Location = new System.Drawing.Point(416, 87);
            this.RoomNameLabel.Name = "RoomNameLabel";
            this.RoomNameLabel.Size = new System.Drawing.Size(71, 15);
            this.RoomNameLabel.TabIndex = 2;
            this.RoomNameLabel.Text = "room name";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.5F);
            this.label1.Location = new System.Drawing.Point(153, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(489, 43);
            this.label1.TabIndex = 3;
            this.label1.Text = "The Hardest Cyber Trivia Challenge";
            // 
            // Answer1Button
            // 
            this.Answer1Button.Location = new System.Drawing.Point(103, 213);
            this.Answer1Button.Name = "Answer1Button";
            this.Answer1Button.Size = new System.Drawing.Size(100, 30);
            this.Answer1Button.TabIndex = 4;
            this.Answer1Button.Text = "Answer1Button";
            this.Answer1Button.UseVisualStyleBackColor = true;
            this.Answer1Button.Click += new System.EventHandler(this.ClickedButton);
            // 
            // Answer2Button
            // 
            this.Answer2Button.Location = new System.Drawing.Point(555, 213);
            this.Answer2Button.Name = "Answer2Button";
            this.Answer2Button.Size = new System.Drawing.Size(100, 30);
            this.Answer2Button.TabIndex = 5;
            this.Answer2Button.Text = "Answer2Button";
            this.Answer2Button.UseVisualStyleBackColor = true;
            this.Answer2Button.Click += new System.EventHandler(this.ClickedButton);
            // 
            // Answer3Button
            // 
            this.Answer3Button.Location = new System.Drawing.Point(103, 353);
            this.Answer3Button.Name = "Answer3Button";
            this.Answer3Button.Size = new System.Drawing.Size(100, 30);
            this.Answer3Button.TabIndex = 6;
            this.Answer3Button.Text = "Answer3Button";
            this.Answer3Button.UseVisualStyleBackColor = true;
            this.Answer3Button.Click += new System.EventHandler(this.ClickedButton);
            // 
            // Answer4Button
            // 
            this.Answer4Button.Location = new System.Drawing.Point(555, 353);
            this.Answer4Button.Name = "Answer4Button";
            this.Answer4Button.Size = new System.Drawing.Size(100, 30);
            this.Answer4Button.TabIndex = 7;
            this.Answer4Button.Text = "Answer4Button";
            this.Answer4Button.UseVisualStyleBackColor = true;
            this.Answer4Button.Click += new System.EventHandler(this.ClickedButton);
            // 
            // NumOfQuestionsLabel
            // 
            this.NumOfQuestionsLabel.AutoSize = true;
            this.NumOfQuestionsLabel.Location = new System.Drawing.Point(357, 125);
            this.NumOfQuestionsLabel.Name = "NumOfQuestionsLabel";
            this.NumOfQuestionsLabel.Size = new System.Drawing.Size(35, 13);
            this.NumOfQuestionsLabel.TabIndex = 8;
            this.NumOfQuestionsLabel.Text = "label2";
            // 
            // QuestionLabel
            // 
            this.QuestionLabel.AutoSize = true;
            this.QuestionLabel.Location = new System.Drawing.Point(187, 157);
            this.QuestionLabel.Name = "QuestionLabel";
            this.QuestionLabel.Size = new System.Drawing.Size(35, 13);
            this.QuestionLabel.TabIndex = 9;
            this.QuestionLabel.Text = "label3";
            // 
            // QuestionTimerLable
            // 
            this.QuestionTimerLable.AutoSize = true;
            this.QuestionTimerLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.QuestionTimerLable.Location = new System.Drawing.Point(356, 289);
            this.QuestionTimerLable.Name = "QuestionTimerLable";
            this.QuestionTimerLable.Size = new System.Drawing.Size(59, 20);
            this.QuestionTimerLable.TabIndex = 10;
            this.QuestionTimerLable.Text = "TIMER";
            // 
            // ScoreLabel
            // 
            this.ScoreLabel.AutoSize = true;
            this.ScoreLabel.Location = new System.Drawing.Point(246, 425);
            this.ScoreLabel.Name = "ScoreLabel";
            this.ScoreLabel.Size = new System.Drawing.Size(47, 13);
            this.ScoreLabel.TabIndex = 11;
            this.ScoreLabel.Text = "SCORE:";
            // 
            // QuestionTimer
            // 
            this.QuestionTimer.Interval = 1000;
            this.QuestionTimer.Tick += new System.EventHandler(this.QuestionTimer_Tick);
            // 
            // triviaGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.question_marks;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 475);
            this.Controls.Add(this.ScoreLabel);
            this.Controls.Add(this.QuestionTimerLable);
            this.Controls.Add(this.QuestionLabel);
            this.Controls.Add(this.NumOfQuestionsLabel);
            this.Controls.Add(this.Answer4Button);
            this.Controls.Add(this.Answer3Button);
            this.Controls.Add(this.Answer2Button);
            this.Controls.Add(this.Answer1Button);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RoomNameLabel);
            this.Controls.Add(this.UserNameLabel);
            this.Controls.Add(this.ExitButton);
            this.Name = "triviaGame";
            this.Text = "Trivia Game";
            this.Load += new System.EventHandler(this.triviaGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.Label RoomNameLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Answer1Button;
        private System.Windows.Forms.Button Answer2Button;
        private System.Windows.Forms.Button Answer3Button;
        private System.Windows.Forms.Button Answer4Button;
        private System.Windows.Forms.Label NumOfQuestionsLabel;
        private System.Windows.Forms.Label QuestionLabel;
        private System.Windows.Forms.Label QuestionTimerLable;
        private System.Windows.Forms.Label ScoreLabel;
        private System.Windows.Forms.Timer QuestionTimer;
    }
}