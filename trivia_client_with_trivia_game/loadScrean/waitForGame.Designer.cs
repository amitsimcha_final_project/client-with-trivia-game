﻿namespace loadScrean
{
    partial class waitForGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.connactionInfoLabel = new System.Windows.Forms.Label();
            this.userNameLable = new System.Windows.Forms.Label();
            this.roomNameLabel = new System.Windows.Forms.Label();
            this.closeRoomButton = new System.Windows.Forms.Button();
            this.startGameButton = new System.Windows.Forms.Button();
            this.playersInRoomPanel = new System.Windows.Forms.Panel();
            this.number_of_questionsLabel = new System.Windows.Forms.Label();
            this.nax_number_playersLabel = new System.Windows.Forms.Label();
            this.time_per_questionLabel = new System.Windows.Forms.Label();
            this.leaveRoomButton = new System.Windows.Forms.Button();
            this.errorLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Orange;
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(29, 204);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(534, 151);
            this.label1.TabIndex = 0;
            // 
            // connactionInfoLabel
            // 
            this.connactionInfoLabel.AutoEllipsis = true;
            this.connactionInfoLabel.AutoSize = true;
            this.connactionInfoLabel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.connactionInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.connactionInfoLabel.Location = new System.Drawing.Point(95, 213);
            this.connactionInfoLabel.Name = "connactionInfoLabel";
            this.connactionInfoLabel.Size = new System.Drawing.Size(231, 17);
            this.connactionInfoLabel.TabIndex = 1;
            this.connactionInfoLabel.Text = "you are connected to the room";
            this.connactionInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // userNameLable
            // 
            this.userNameLable.AutoSize = true;
            this.userNameLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.userNameLable.Location = new System.Drawing.Point(21, 24);
            this.userNameLable.Name = "userNameLable";
            this.userNameLable.Size = new System.Drawing.Size(112, 18);
            this.userNameLable.TabIndex = 2;
            this.userNameLable.Text = "userNameLable";
            // 
            // roomNameLabel
            // 
            this.roomNameLabel.AutoSize = true;
            this.roomNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.roomNameLabel.Location = new System.Drawing.Point(444, 24);
            this.roomNameLabel.Name = "roomNameLabel";
            this.roomNameLabel.Size = new System.Drawing.Size(119, 18);
            this.roomNameLabel.TabIndex = 3;
            this.roomNameLabel.Text = "roomNameLable";
            // 
            // closeRoomButton
            // 
            this.closeRoomButton.Location = new System.Drawing.Point(24, 421);
            this.closeRoomButton.Name = "closeRoomButton";
            this.closeRoomButton.Size = new System.Drawing.Size(75, 23);
            this.closeRoomButton.TabIndex = 4;
            this.closeRoomButton.Text = "close room";
            this.closeRoomButton.UseVisualStyleBackColor = true;
            this.closeRoomButton.Click += new System.EventHandler(this.closeRoomButton_Click);
            // 
            // startGameButton
            // 
            this.startGameButton.Location = new System.Drawing.Point(462, 421);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(75, 23);
            this.startGameButton.TabIndex = 5;
            this.startGameButton.Text = "start game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // playersInRoomPanel
            // 
            this.playersInRoomPanel.AutoScroll = true;
            this.playersInRoomPanel.Location = new System.Drawing.Point(197, 233);
            this.playersInRoomPanel.Name = "playersInRoomPanel";
            this.playersInRoomPanel.Size = new System.Drawing.Size(200, 100);
            this.playersInRoomPanel.TabIndex = 6;
            // 
            // number_of_questionsLabel
            // 
            this.number_of_questionsLabel.AutoSize = true;
            this.number_of_questionsLabel.Location = new System.Drawing.Point(95, 177);
            this.number_of_questionsLabel.Name = "number_of_questionsLabel";
            this.number_of_questionsLabel.Size = new System.Drawing.Size(111, 13);
            this.number_of_questionsLabel.TabIndex = 7;
            this.number_of_questionsLabel.Text = "number_of_questions:";
            // 
            // nax_number_playersLabel
            // 
            this.nax_number_playersLabel.AutoSize = true;
            this.nax_number_playersLabel.Location = new System.Drawing.Point(243, 177);
            this.nax_number_playersLabel.Name = "nax_number_playersLabel";
            this.nax_number_playersLabel.Size = new System.Drawing.Size(109, 13);
            this.nax_number_playersLabel.TabIndex = 8;
            this.nax_number_playersLabel.Text = "max_number_players:";
            // 
            // time_per_questionLabel
            // 
            this.time_per_questionLabel.AutoSize = true;
            this.time_per_questionLabel.Location = new System.Drawing.Point(381, 177);
            this.time_per_questionLabel.Name = "time_per_questionLabel";
            this.time_per_questionLabel.Size = new System.Drawing.Size(96, 13);
            this.time_per_questionLabel.TabIndex = 9;
            this.time_per_questionLabel.Text = "time_per_question:";
            // 
            // leaveRoomButton
            // 
            this.leaveRoomButton.Location = new System.Drawing.Point(24, 421);
            this.leaveRoomButton.Name = "leaveRoomButton";
            this.leaveRoomButton.Size = new System.Drawing.Size(75, 23);
            this.leaveRoomButton.TabIndex = 10;
            this.leaveRoomButton.Text = "leave room";
            this.leaveRoomButton.UseVisualStyleBackColor = true;
            this.leaveRoomButton.Click += new System.EventHandler(this.leaveRoomButton_Click);
            // 
            // errorLabel
            // 
            this.errorLabel.AutoSize = true;
            this.errorLabel.Location = new System.Drawing.Point(210, 336);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(28, 13);
            this.errorLabel.TabIndex = 11;
            this.errorLabel.Text = "error";
            this.errorLabel.Visible = false;
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(251, 374);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 12;
            this.okButton.Text = "ok";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Visible = false;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // waitForGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::loadScrean.Properties.Resources.שעונים_מעוררים;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(588, 483);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.leaveRoomButton);
            this.Controls.Add(this.time_per_questionLabel);
            this.Controls.Add(this.nax_number_playersLabel);
            this.Controls.Add(this.number_of_questionsLabel);
            this.Controls.Add(this.playersInRoomPanel);
            this.Controls.Add(this.startGameButton);
            this.Controls.Add(this.closeRoomButton);
            this.Controls.Add(this.roomNameLabel);
            this.Controls.Add(this.userNameLable);
            this.Controls.Add(this.connactionInfoLabel);
            this.Controls.Add(this.label1);
            this.Name = "waitForGame";
            this.Text = "Wait For Game";
            this.Load += new System.EventHandler(this.waitForGame_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label connactionInfoLabel;
        private System.Windows.Forms.Label userNameLable;
        private System.Windows.Forms.Label roomNameLabel;
        private System.Windows.Forms.Button closeRoomButton;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.Panel playersInRoomPanel;
        private System.Windows.Forms.Label number_of_questionsLabel;
        private System.Windows.Forms.Label nax_number_playersLabel;
        private System.Windows.Forms.Label time_per_questionLabel;
        private System.Windows.Forms.Button leaveRoomButton;
        private System.Windows.Forms.Label errorLabel;
        private System.Windows.Forms.Button okButton;
    }
}