﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Threading;

namespace loadScrean
{
    public partial class bestScore : Form
    {
        public bestScore(string currUsername, string place1Name, string place1Score,
            string place2Name, string place2Score, string place3Name, string place3Score)
        {
            InitializeComponent();
            
            // init the screen.
            currnameLabel.Text = currUsername;
            place1Label.Text = " !עם " + Convert.ToInt32(place1Score) + " נקודות " + place1Name;
            place2Label.Text = " !עם " + Convert.ToInt32(place2Score) + " נקודות " + place2Name;
            place3Label.Text = " !עם " + Convert.ToInt32(place3Score) + " נקודות " + place3Name;
        }
    

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            gargamel gargamel = new gargamel();
            gargamel.ShowDialog();
            this.Show();
        }
    }
}
